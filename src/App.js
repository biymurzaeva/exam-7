import {faUtensils, faCoffee, faHamburger, faHotdog, faPizzaSlice, faMugHot, faDrumstickBite} from "@fortawesome/free-solid-svg-icons";
import Items from "./components/Items/Items";
import './App.css';
import {useState} from "react";
import Details from "./components/Details/Details";
import {nanoid} from 'nanoid';

const App = () => {
    const ITEMS = [
        {name: 'Hamburger', price: 80, id: 0, icon: faHamburger},
        {name: 'Cheeseburger', price: 90, id: 1, icon: faUtensils},
        {name: 'Fries', price: 45, id: 2, icon: faUtensils},
        {name: 'Coffee', price: 70, id: 3, icon: faCoffee},
        {name: 'Tea', price: 50, id: 4, icon: faMugHot},
        {name: 'Cola', price: 40, id: 5, icon: faCoffee},
        {name: 'Hotdog', price: 45, id: 6, icon: faHotdog},
        {name: 'Pizza', price: 30, id: 7, icon: faPizzaSlice},
        {name: 'Chicken', price: 85, id: 8, icon: faDrumstickBite},
    ];

    const [details, setDetails] = useState([]);

    const total = details.reduce((acc, item) => acc + (item.price * item.count), 0);

    const [showOrders, setShowOrders] = useState(false);

    const addDetails = obj => {
        setDetails([...details, {name: obj.name, count: 1, price: obj.price, id: nanoid()}]);

        if (details.length !== 0) {
            details.forEach(d => {
                if (d.name === obj.name) {
                    setDetails(details.map((detal) => {
                        if (obj.name === detal.name) {
                            return {
                                ...detal,
                                count: detal.count + 1
                            };
                        }

                        return detal;
                    }));
                }
            });
        }

        setShowOrders(true);
    };

    const removeDetail = id => {
        setDetails(details.filter(p => p.id !== id));
    };


    let orders;

    if (showOrders) {
        orders = (
            <Details
                details={details}
                removeDetail={removeDetail}
                total={total}
            />
        );
    } else {
        orders = (
            <div>
                <p>Order is empty!</p>
                <p>Please add some items!</p>
            </div>
        );
    }

    return (
        <div className="container">
            <div className="fast-food-dask">
                <div className="details-block">
                    <fieldset className="fieldset">
                        <legend>Order Details</legend>
                        {orders}
                    </fieldset>
                </div>
                <div className="items-block">
                    <fieldset>
                        <legend>Add items</legend>
                        <Items
                            items={ITEMS}
                            addDetail={addDetails}

                        />
                    </fieldset>
                </div>
            </div>
        </div>
    );
};

export default App;
