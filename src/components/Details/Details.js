import React from 'react';
import Detail from "../Detail/Detail";


const Details = props => {
    return (
        <div>
            {props.details.map((d) => (
                <Detail
                    key={d.id}
                    name={d.name}
                    price={d.price}
                    count={d.count}
                    del={() => {props.removeDetail(d.id)}}
                />
            ))}
            <p><strong>Total: </strong>{props.total} KGS</p>
        </div>
    );
};

export default Details;