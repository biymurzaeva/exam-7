import React from 'react';
import './Detail.css';

const Detail = props => {
    return (
        <p>
            <span>{props.name} </span>
            <span> x{props.count} </span>
            <span>{props.price} KGS</span>
            <button onClick={props.del} className="reset-btn">x</button>
        </p>
    );
};

export default Detail;