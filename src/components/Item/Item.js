import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import './Item.css';

const Item = props => {
    return (
        <div className="item-block" onClick={props.addToDetails}>
            <div>
                <FontAwesomeIcon icon={props.icon} size="3x" className="icon"/>
            </div>
            <div>
                <h4>{props.name}</h4>
                <p>Price: {props.price} KGS</p>
            </div>
        </div>
    );
};

export default Item;