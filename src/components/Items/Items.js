import React from 'react';
import Item from "../Item/Item";
import './Items.css';

const Items = props => {
    const addItems = props.items.map((ing)=> (
        <Item
            key={ing.id}
            name={ing.name}
            price={ing.price}
            icon={ing.icon}
            addToDetails={() => props.addDetail(ing)}
        />
    ));
    return (
        <div className="items">
            {addItems}
        </div>
    );
};

export default Items;